package com.microservice.fornecedor.repository;

import org.springframework.data.repository.CrudRepository;

import com.microservice.fornecedor.model.Pedido;

public interface PedidoRepository extends CrudRepository<Pedido, Long>{

}
